import sys
from PySide import QtGui, QtCore


# Test branching.

# names = ["Iniesta", "Xavi", "Iniesta", "Xavi", "Iniesta", "Xavi", "Iniesta", "Xavi", "Iniesta", "Xavi", "Iniesta"]

names = ["Iniesta", "Xavi"]


redPen = QtGui.QPen(QtGui.QColor("red"),5)
grePen = QtGui.QPen(QtGui.QColor("blue"),5)
yelPen = QtGui.QPen(QtGui.QColor("Yellow"),5)

class Player(QtGui.QGraphicsPixmapItem):
    
    def hoverEnterEvent(self, e):   
        print "Entered: " + self.id
        self.drawBorder(yelPen)
        self.drag = 0
        
    def mousePressEvent(self, e):
        
        if e.button() == QtCore.Qt.LeftButton:
            print "Pressed"
            self.drawBorder(grePen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ClosedHandCursor)
            self.diffXY = e.scenePos() - self.scenePos()
            self.drag = 1

        
    def mouseReleaseEvent(self, e):
        if e.button() == QtCore.Qt.LeftButton:
            print "Released"
            self.drawBorder(yelPen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ArrowCursor)
            self.drawBorder(yelPen)
            self.drag = 0
            
    def mouseMoveEvent(self, e):
        if self.drag:
            self.setPos(e.scenePos() - self.diffXY)
            self.rect.setPos(e.scenePos() - self.diffXY)
            
    def hoverLeaveEvent(self, e):
        print "Left"
        print "--"
        self.drawBorder(redPen)
        
    def drawBorder(self,pen):
        if self.rect:
            scene.removeItem(self.rect)
        
        self.br = self.boundingRect()
        self.rect = QtGui.QGraphicsRectItem(self.br)
        self.rect.setPos(self.pos())
        self.rect.setPen(pen)
        scene.addItem(self.rect)



if __name__ == '__main__':
    
    app = QtGui.QApplication(sys.argv)
    scene = QtGui.QGraphicsScene()
    
    scene.addItem(QtGui.QGraphicsPixmapItem("Images\campo.png"))

    # Populates scene
    for i in range(len(names)):
        item = Player("Images\p" + names[i] + ".png")
        item.setAcceptHoverEvents(True)
        item.setAcceptDrops(True)
        item.setPos(i*100+100,270)
        item.rect = 0
        item.drawBorder(redPen)
        item.id = names[i]
        scene.addItem(item)


    # Creates View
    view = QtGui.QGraphicsView(scene)
    # Sets basic Flags for nice rendering 
    view.setRenderHints(QtGui.QPainter.Antialiasing or QtGui.QPainter.SmoothPixmapTransform)

    view.show()
    sys.exit(app.exec_())