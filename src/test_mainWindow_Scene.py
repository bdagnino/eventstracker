import sys
from PySide import QtGui, QtCore


redPen = QtGui.QPen(QtGui.QColor("red"),5)
grePen = QtGui.QPen(QtGui.QColor("blue"),5)
yelPen = QtGui.QPen(QtGui.QColor("Yellow"),5)

class Player(QtGui.QGraphicsPixmapItem):
    
    def hoverEnterEvent(self, e):   
        print "Entered: " + self.id
        self.drawBorder(yelPen)
        self.drag = 0
        
    def mousePressEvent(self, e):
        
        if e.button() == QtCore.Qt.LeftButton:
            print "Pressed"
            self.drawBorder(grePen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ClosedHandCursor)
            self.diffXY = e.scenePos() - self.scenePos()
            self.drag = 1

        
    def mouseReleaseEvent(self, e):
        if e.button() == QtCore.Qt.LeftButton:
            print "Released"
            self.drawBorder(yelPen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ArrowCursor)
            self.drawBorder(yelPen)
            self.drag = 0
            
    def mouseMoveEvent(self, e):
        if self.drag:
            self.setPos(e.scenePos() - self.diffXY)
            self.rect.setPos(e.scenePos() - self.diffXY)
            
    def hoverLeaveEvent(self, e):
        print "Left"
        print "--"
        self.drawBorder(redPen)
        
    def drawBorder(self,pen):
        if self.rect:
            mainWindow.scene.removeItem(self.rect)
        
        self.br = self.boundingRect()
        self.rect = QtGui.QGraphicsRectItem(self.br)
        self.rect.setPos(self.pos())
        self.rect.setPen(pen)
        mainWindow.scene.addItem(self.rect)


class playerIcon(QtGui.QLabel):
        
        def __init__(self):
            super(playerIcon,self).__init__()
            self.setPixmap(QtGui.QPixmap("Images\generic_player.png"))

        def mousePressEvent(self, e):
            if e.button() == QtCore.Qt.LeftButton:
                print "Pressed Left"
            elif e.button() == QtCore.Qt.RightButton:
                print "Pressed Right"
            
class GraphScene(QtGui.QGraphicsScene):
    
    def __init__(self, parent = None):
        super(GraphScene,self).__init__(parent)
        self.addItem(QtGui.QGraphicsPixmapItem("Images\campo.png"))

class MainWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(MainWindow,self).__init__()
        
        layout = QtGui.QHBoxLayout()
        
        self.pbTrack = QtGui.QPushButton('Add Player', self)
        self.pbTrack.resize(self.pbTrack.sizeHint())
        layout.addWidget(self.pbTrack)
        
        self.lblPlayer = playerIcon()
        layout.addWidget(self.lblPlayer)
        
        self.scene = GraphScene()
        self.view = QtGui.QGraphicsView(self.scene)
        layout.addWidget(self.view)
        
        self.widget = QtGui.QWidget()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)

        self.players = []
        
    def printsomething(self):
        print "Hello"

if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)

    mainWindow = MainWindow()
    mainWindow.setGeometry(50, 50, 1200, 700)
    mainWindow.show()

    sys.exit(app.exec_())
