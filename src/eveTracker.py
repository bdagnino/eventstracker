import sys
import cv2
import cv2.cv as cv
from   PySide import QtGui, QtCore
import numpy as np
import json
import time
import scipy.io


# Global parameters.
teamsName = ["Badalona", "Alcoyano"]

pXRow    = 5
nPlayers = 14

pKeys = np.array([0,
                  67, 65, 83, 68, 70, 
                  81, 87, 69, 82, 50, 
                  51, 86, 71, 84, 
                  77, 186, 76, 75, 74,
                  80, 79, 73, 85, 56,
                  57, 78, 72, 89,
                  66])

pCode = np.array([0,
                  101, 102, 103, 104, 105, 
                  106, 107, 108, 109, 110,
                  111, 112, 113, 114,
                  201, 202, 203, 204, 205, 
                  206, 207, 208, 209, 210,
                  211, 212, 213, 214,
                  -100])

pTeam = np.array([0, 
                  1, 1, 1, 1, 1, 
                  1, 1, 1, 1, 1,
                  1, 1, 1, 1,
                  2, 2, 2, 2, 2, 
                  2, 2, 2, 2, 2,
                  2, 2, 2, 2,
                  -1])

pInds = np.array([0, 
                  1, 2, 3, 4, 5, 
                  6, 7, 8, 9, 10,
                  11, 12, 13, 14,
                  1, 2, 3, 4, 5, 
                  6, 7, 8, 9, 10,
                  11, 12, 13, 14,
                  -1])

W = 1544     
H = 228

bFrameStep  = 25
sFrameStep  = 10

redPen = QtGui.QPen(QtGui.QColor("red"),4)
redPenF = QtGui.QPen(QtGui.QColor("red"),4)
grePen = QtGui.QPen(QtGui.QColor("blue"),4)
grePenF = QtGui.QPen(QtGui.QColor("blue"),4)
yelPen = QtGui.QPen(QtGui.QColor("Yellow"),4)
blaPen = QtGui.QPen(QtGui.QColor("black"),4)

class Teams(object):
    
    def __init__(self,Name,ID):
        self.Name       = Name
        self.id         = ID
        self.nPlayers   = nPlayers
        self.Players    = []
        self.Icons      = []
        self.Formation  = "2-2"
        self.Images     = []
        for i in range(self.nPlayers):
            self.Images.append("Images\generic_player.png")
                
class playerIcon(QtGui.QLabel):
        
        def __init__(self):
            super(playerIcon,self).__init__()
            self.fname =  ""
            self.Team = []
            self.setPixmap(QtGui.QPixmap("Images\generic_player.png"))
            self.setFrameStyle(QtGui.QFrame.Box)
            self.setLineWidth(2)


        def mousePressEvent(self, e):
            if e.button() == QtCore.Qt.LeftButton:
                if self.fname is not "": 
                    if ex.Teams[self.Team-1].Players[self.itemID-1].inScene:
                        ex.rmPlayer(self.Team, self.itemID)
                        self.setLineWidth(2)
                    else:
                        ex.addPlayer(self.Team, self.itemID)
                        self.setLineWidth(2)
                        self.setFrameStyle(QtGui.QFrame.Box)

                
            elif e.button() == QtCore.Qt.RightButton:
                fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Select player I','C:\Users\Bruno\workspace')
                if fname:
                    self.fname = fname
                    self.setPixmap(QtGui.QPixmap(fname))
                    ex.Teams[self.Team-1].Players[self.itemID-1].setPixmap(fname)
                    info = QtCore.QFileInfo(fname)
                    ex.Teams[self.Team-1].Players[self.itemID-1].Name = info.baseName()
                    ex.Teams[self.Team-1].Players[self.itemID-1].Image = fname
                    ex.Teams[self.Team-1].Images[self.itemID-1]        = fname
                                                         
class Player(QtGui.QGraphicsPixmapItem):
    
    def __init__(self,pixmap):
        super(Player,self).__init__()
        self.setPixmap(pixmap)
        self.Image = pixmap
        self.Team = ""
        self.Name = ""
        self.itemID = []
        self.id   = []
        self.inScene = 0
        self.drag = 0
#     def hoverEnterEvent(self, e):   
#         print "Entered: " + self.Name + "(" + str(self.id) + ")"
#         self.drawBorder(yelPen)
#         self.drag = 0
#         ex.Video.playerPoss = self.id

#     def hoverLeaveEvent(self, e):
#             print "Left"
#             print "--"
#             self.drawBorder(blaPen)
#             ex.Video.playerPoss = -100        

    def mousePressEvent(self, e):
        
        if e.button() == QtCore.Qt.LeftButton:
            print "Pressed"
            self.drawBorder(grePen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ClosedHandCursor)
            self.diffXY = e.scenePos() - self.scenePos()
            self.drag = 1

    def mouseReleaseEvent(self, e):
        if e.button() == QtCore.Qt.LeftButton:
            print "Released"
            self.drawBorder(redPen)
            
        elif e.button() == QtCore.Qt.RightButton:
            self.setCursor(QtCore.Qt.ArrowCursor)
            self.drawBorder(redPen)
            self.drag = 0
            
    def mouseMoveEvent(self, e):
        if self.drag:
            self.setPos(e.scenePos() - self.diffXY)
            if self.inScene:
                self.rect.setPos(e.scenePos() - self.diffXY)
            
    def drawBorder(self,pen):
        if not self.inScene:
            if self.rect:
                ex.Scene.removeItem(self.rect)
                self.rect = 0
            
        else:
            if self.rect:
                ex.Scene.removeItem(self.rect)
            
            self.br = self.boundingRect()
            self.rect = QtGui.QGraphicsRectItem(self.br)
            self.rect.setPos(self.pos())
            self.rect.setPen(pen)
            ex.Scene.addItem(self.rect)
            
class Project(object):
    def __init__( self ):
        self.File = None
        self.Name = None
        self.Path = None

class VideoRep(QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.Project    = Project()
        self.file       = []
        self.Status     = 0
        self.Run        = 1
        self.newFile    = 0 
        self.succ       = 0
        self.nPlayers   = 0
        self.video      = 0
        self.frame      = -1
        self.chgFrame   = 0
        self.setMCall   = 1
        self.defFiles   = 1
        self.lastFrame  = 0
        self.resizeWH   = [W,H]
        self.time1      = 0
        self.fps        = 25
        self.spFactor   = 1
        self.playerPoss = -100
        self.ballInField= 0
        self.Possession = []
        self.matFile    = ""
        
        
    def __del__(self):
        self.wait()      
        
    def run(self):  
        
        while self.Run:

            if self.frame>self.lastFrame:
                self.lastFrame = self.frame
            
            if self.newFile:
                self.newFile = 0
                self.file = self.newFileName
                self.video = cv2.VideoCapture(self.file)
                cv2.namedWindow('img',cv.CV_WINDOW_NORMAL)
                cv2.resizeWindow('img', W, H)
                cv2.moveWindow('img', 20, 20)
                self.nFrames        = self.video.get(cv.CV_CAP_PROP_FRAME_COUNT)
#                 print self.video.get(cv.CV_CAP_PROP_FRAME_HEIGHT)
#                 print self.video.get(cv.CV_CAP_PROP_FRAME_WIDTH)
                self.fps     = self.video.get(cv.CV_CAP_PROP_FPS)
                self.sldVideo.setRange(0,self.nFrames)
                self.sldVideo.setValue(self.frame)
                self.video.set(cv.CV_CAP_PROP_POS_FRAMES,self.frame)
                self.succ, self.image = self.video.read()
                self.frame = self.video.get(cv.CV_CAP_PROP_POS_FRAMES)
                self.image = cv2.resize(self.image,(self.resizeWH[0],self.resizeWH[1]))
                self.imageOriginal  = self.image.copy()
                self.Possession  = 0*np.ones((self.nFrames,1))

        
            if self.Status and self.newFile is not 1:
                self.succ, self.image = self.video.read()
                self.frame = self.video.get(cv.CV_CAP_PROP_POS_FRAMES)
                self.sldVideo.setValue(self.frame)
                self.image = cv2.resize(self.image,(self.resizeWH[0],self.resizeWH[1]))
                self.imageOriginal  = self.image.copy() 
            
            if self.chgFrame:
                self.chgFrame = 0
                if self.video is not 0:
                    self.video.set(cv.CV_CAP_PROP_POS_FRAMES,self.frame)
                    self.succ, self.image = self.video.read()
                    self.sldVideo.setValue(self.frame)
                    self.image = cv2.resize(self.image,(self.resizeWH[0],self.resizeWH[1]))
                    self.imageOriginal  = self.image.copy()
                
                    
            if self.succ:
                
                msTime =  self.video.get(cv.CV_CAP_PROP_POS_MSEC)
                MIN    = str(int((msTime/1000)/60))
                SEC    = str(int((msTime/1000)%60))
                if len(MIN) == 1:
                    MIN = '0' + MIN
                
                if len(SEC) == 1:
                    SEC = '0' + SEC
                    
                strTime = MIN + ':' + SEC
                self.lblTime.setText(strTime)
                
                if self.time1 is not 0:
                    tDiff = self.spFactor*1.0/self.fps -(time.time() - self.time1)
                    if tDiff > 0:
                        time.sleep(tDiff)
                    
#                 print time.time() - self.time1
                self.time1 = time.time()
                cv2.imshow('img',self.image)
                self.Possession[self.frame-1,0] = self.playerPoss
                cv.WaitKey(1)
                if self.setMCall:
                    cv.SetMouseCallback('img', self.on_mouse, 0)
                    self.setMCall = 0
                
                if self.matFile is not "":
                    scipy.io.savemat(self.matFile, dict(X = self.Possession))
                    np.save(self.numPyFile,self.Possession)
   
    def on_mouse(self,event, x, y, flags, params):
        
        if event == cv.CV_EVENT_MBUTTONDOWN:
                
            if flags is 36:
                
                if self.Status:
                    self.Status = 0
                    self.pbTrack.setText("TRACK")
                else:
                    self.Status = 1
                    self.pbTrack.setText("STOP")
    
        elif event == cv.CV_EVENT_LBUTTONDOWN and self.Status is 0:
            self.chgFrame   = 1
            if flags is 33:
                self.frame  = self.frame - bFrameStep
            else:
                self.frame  = self.frame - sFrameStep
                
            if self.frame < 0:
                self.frame  = 0
                
        elif event == cv.CV_EVENT_RBUTTONDOWN and self.Status is 0:
            self.chgFrame   = 1
            if flags is 34:
                self.frame  = self.frame + bFrameStep
            else:
                self.frame  = self.frame + sFrameStep
                
            if self.frame > self.nFrames:
                self.frame  = self.nFrames
                
class footballField(QtGui.QGraphicsPixmapItem):
    
    def __init__(self,pixmap):
        super(footballField,self).__init__()
        self.setPixmap(pixmap)
        self.setAcceptHoverEvents(True)
        
        
    def drawBorder(self,pen):
            
            self.br = self.boundingRect()
            self.rect = QtGui.QGraphicsRectItem(self.br)
            self.rect.setPos(self.pos())
            self.rect.setPen(pen)
            ex.Scene.addItem(self.rect)
            
#     def hoverEnterEvent(self, e):   
#         print "Entered Field"
# #         ex.Video.ballInField = 1
#         if ex.Video.frame is not -1:
#             XY = e.scenePos()
#             ex.Video.Possession[ex.Video.frame, 1] = XY.x()
#             ex.Video.Possession[ex.Video.frame, 2] = XY.y()
#         
#     def hoverLeaveEvent(self,e):
#         print "Left Field"
# #         ex.Video.ballInField = 0
#         if ex.Video.frame is not -1:
#             XY = e.scenePos()
#             ex.Video.Possession[ex.Video.frame, 1] = XY.x()
#             ex.Video.Possession[ex.Video.frame, 2] = XY.y()
            
    def mousePressEvent(self, e):
        
        if e.button() == QtCore.Qt.MiddleButton:
            if ex.Video.Status:
                ex.Video.Status = 0
                ex.Video.pbTrack.setText("TRACK")
            else:
                ex.Video.Status = 1
                ex.Video.pbTrack.setText("STOP")
        
        elif e.button() == QtCore.Qt.LeftButton and ex.Video.Status is 0:
            ex.Video.chgFrame   = 1
            ex.Video.frame  = ex.Video.frame - sFrameStep
                
            if ex.Video.frame < 0:
                ex.Video.frame  = 0
            
        elif e.button() == QtCore.Qt.RightButton:
            ex.Video.chgFrame   = 1
            ex.Video.frame  = ex.Video.frame + sFrameStep
                
            if ex.Video.frame > ex.Video.nFrames:
                ex.Video.frame  = ex.Video.nFrames
                
class graphicScene(QtGui.QGraphicsScene):
    
    def __init__(self, parent = None):
        super(graphicScene,self).__init__(parent)
        self.Field = footballField("Images\campo.png")
        self.addItem(self.Field)
        
class teamLayouts(object):
    
    def __init__(self):
        self.Teams = []
        self.Names = []
        self.Icons = []

class eventsTracker(QtGui.QMainWindow):
    
    def __init__(self):
        super(eventsTracker, self).__init__()
        
        self.spacers     = []
        self.teamLayouts = teamLayouts()
        self.Teams       = []
        self.Pressed     = -1
        
        self.Video = VideoRep()
        self.Video.start()
        
        self.statusBar().showMessage('Ready')
        
        self.mainWidget = QtGui.QWidget()
        self.setCentralWidget(self.mainWidget)
        
        self.setGeometry(30, 120, 1190, 600)
        self.setWindowTitle('MQF Tracker')
        self.setMinimumWidth(150) 
        
        
        self.openFile = QtGui.QAction(QtGui.QIcon('Images\load_video1.png'), 'Open Video', self)
        self.openFile.setShortcut('Ctrl+O')
        self.openFile.triggered.connect(self.showDialog)
        self.toolbar = self.addToolBar('Open')
        self.toolbar.addAction(self.openFile)
        
        self.saveProject = QtGui.QAction(QtGui.QIcon('Images\save_project.png'), 'Save Project', self)
        self.saveProject.setShortcut('Ctrl+S')
        self.saveProject.triggered.connect(self.saveP)
        self.toolbar = self.addToolBar('Save')
        self.toolbar.addAction(self.saveProject)
        
        self.loadProject = QtGui.QAction(QtGui.QIcon('Images\load_project.png'), 'Load Project', self)
        self.loadProject.setShortcut('Ctrl+L')
        self.loadProject.triggered.connect(self.loadP)
        self.toolbar = self.addToolBar('Load')
        self.toolbar.addAction(self.loadProject)
        
        self.globalLayout   = QtGui.QHBoxLayout() 
        
        self.mainWidget.layout = QtGui.QVBoxLayout()
        
        self.mainWidget.row0   = QtGui.QHBoxLayout()       
        f = QtGui.QFont()
        f.setFamily('Verdana')
        f.setBold(True)
        f.setPointSize(12)
        self.Title  = QtGui.QLabel('MQF Events System', self)
        self.Title.setFont(f)
        self.mainWidget.row0.addStretch(1)
        self.mainWidget.row0.addWidget(self.Title)
        self.mainWidget.row0.addStretch(1)
        
        self.mainWidget.row1   = QtGui.QHBoxLayout()
        self.Video.pbTrack = QtGui.QPushButton('PLAY', self)
        self.Video.pbTrack.resize(self.Video.pbTrack.sizeHint())
        self.Video.pbTrack.clicked.connect(self.PlayStopVideo)  
        self.pbExit = QtGui.QPushButton('Exit', self)
        self.pbExit.setShortcut('Ctrl+Q')
        self.pbExit.clicked.connect(self.close)
        self.pbExit.resize(self.pbExit.sizeHint())     
        self.mainWidget.row1.addWidget(self.Video.pbTrack)
        self.mainWidget.row1.addWidget(self.pbExit)
        
        
        self.mainWidget.rowVideoBar = QtGui.QHBoxLayout()
        f = QtGui.QFont()
        f.setFamily('Verdana')
        f.setBold(True)
        f.setPointSize(8)
        self.lblVid  = QtGui.QLabel('Video', self)
        self.Video.sldVideo = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.Video.sldVideo.setFocusPolicy(QtCore.Qt.NoFocus)
        self.Video.sldVideo.setRange(0,99)
        self.Video.sldVideo.sliderReleased.connect(self.setFrame)
        self.Video.lblTime = QtGui.QLabel('00:00', self)
        self.mainWidget.rowVideoBar.addWidget(self.lblVid)
        self.mainWidget.rowVideoBar.addWidget(self.Video.sldVideo)
        self.mainWidget.rowVideoBar.addWidget(self.Video.lblTime)
        
        
        self.mainWidget.rowVideoSpeed = QtGui.QHBoxLayout()
        f = QtGui.QFont()
        f.setFamily('Verdana')
        f.setBold(True)
        f.setPointSize(8)
        self.lblSpeed  = QtGui.QLabel('Decrease speed by:', self)
        self.sldSpeed = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sldSpeed.setFocusPolicy(QtCore.Qt.NoFocus)
        self.sldSpeed.setRange(1,5)
        self.lcdSpeed = QtGui.QLCDNumber()
        self.lcdSpeed.setNumDigits(3)
        self.lcdSpeed.setSegmentStyle(QtGui.QLCDNumber.Flat)
        self.lcdSpeed.display(1)
        self.sldSpeed.valueChanged.connect(self.setSpeed)
        self.mainWidget.rowVideoSpeed.addWidget(self.lblSpeed)
        self.mainWidget.rowVideoSpeed.addWidget(self.sldSpeed)
        self.mainWidget.rowVideoSpeed.addWidget(self.lcdSpeed)
        
        
        for team in range(len(teamsName)):
            self.spacers.append(QtGui.QSpacerItem(10,20))
            self.Teams.append(Teams(teamsName[team],team+1))      
            self.setTeams(self.Teams[team])
        
        

        self.mainWidget.layout.addLayout(self.mainWidget.row0)
        self.mainWidget.layout.addLayout(self.mainWidget.row1)
        self.mainWidget.layout.addLayout(self.mainWidget.rowVideoBar)
        self.mainWidget.layout.addLayout(self.mainWidget.rowVideoSpeed)
        
        for team in range(len(teamsName)):
            self.mainWidget.layout.addItem(self.spacers[team])
            self.mainWidget.layout.addLayout(self.teamLayouts.Teams[team])
        
        self.mainWidget.layout.addStretch(1)
        
        
        self.sceneLayout    = QtGui.QHBoxLayout()
        self.Scene          = graphicScene()
        self.Scene.view     = QtGui.QGraphicsView(self.Scene)
        self.sceneLayout.addWidget(self.Scene.view)
        
        self.globalLayout.addLayout(self.mainWidget.layout)
        self.globalLayout.addLayout(self.sceneLayout)
        
        self.mainWidget.setLayout(self.globalLayout)
        
        self.setCentralWidget(self.mainWidget)
        
    def keyPressEvent(self,e):
        iTemp = np.argmax(pKeys == e.key())
        if iTemp > 0:
            iP = np.argmax(pKeys == e.key()) 
                   
            oldPressed = self.Pressed
            
            if self.Pressed == -1 or self.Pressed != iP:
                self.Pressed = iP
                self.Video.playerPoss = pCode[iP]
                self.Teams[pTeam[iP]-1].Players[pInds[iP]-1].drawBorder(grePen)
                
            elif self.Pressed == iP:
    
                self.Pressed = -1
                self.Video.playerPoss = 0
                self.Scene.Field.drawBorder(grePen)

            if self.Video.playerPoss == 0:
                self.Teams[pTeam[oldPressed]-1].Players[pInds[oldPressed]-1].drawBorder(redPen)
                self.Scene.Field.drawBorder(grePenF)
            
            elif self.Video.playerPoss == -100:
                if oldPressed > 0:
                    self.Teams[pTeam[oldPressed]-1].Players[pInds[oldPressed]-1].drawBorder(redPen)
                    
                self.Scene.Field.drawBorder(redPenF)
                
            else:
                self.Teams[pTeam[oldPressed]-1].Players[pInds[oldPressed]-1].drawBorder(redPen)
                self.Teams[pTeam[iP]-1].Players[pInds[iP]-1].drawBorder(grePen)
                self.Scene.Field.drawBorder(grePenF)

        if e.key() == 32:
            self.PlayStopVideo()
            
        elif e.key() == 86 and self.Video.Status is 0:
            self.Video.chgFrame   = 1
            self.Video.frame  = self.Video.frame - sFrameStep
                
            if self.Video.frame < 1:
                self.Video.frame  = 1
            
        elif e.key() == 78 and self.Video.Status is 0:
            self.Video.chgFrame   = 1
            self.Video.frame  = self.Video.frame + sFrameStep
                
            if self.Video.frame > self.Video.nFrames:
                self.Video.frame  = self.Video.nFrames
        
    def rmPlayer(self,team,itemID):
        self.Teams[team-1].Players[itemID-1].inScene = 0
        self.Scene.removeItem(self.Teams[team-1].Players[itemID-1])
        self.Teams[team-1].Players[itemID-1].drawBorder(redPen)
        
    def addPlayer(self,team,itemID):
        self.Scene.addItem(self.Teams[team-1].Players[itemID-1])
        self.Teams[team-1].Players[itemID-1].inScene = 1
        self.Teams[team-1].Players[itemID-1].drawBorder(redPen)
#         pKeys[1+(team-1)*14+itemID] = 49;
#         pCode[1+(team-1)*14+itemID] = 100*team+itemID;
#         pTeam[1+(team-1)*14+itemID] = team;
#         pInds[1+(team-1)*14+itemID] = itemID;
#         print pKeys
#         print pCode
#         print pTeam
#         print pInds
        
        
    def setTeams(self,Team):
        
        self.teamLayouts.Teams.append(QtGui.QVBoxLayout())
        
        self.teamLayouts.Names.append(QtGui.QHBoxLayout())
        f = QtGui.QFont()
        f.setFamily('Verdana')
        f.setBold(True)
        f.setPointSize(10)
        self.teamLayouts.Names[-1].addStretch(1)
        self.Team  = QtGui.QLabel(Team.Name, self)
        self.Team.setFont(f)
        self.teamLayouts.Names[-1].addWidget(self.Team)
        self.teamLayouts.Names[-1].addStretch(1)
        
        self.teamLayouts.Teams[-1].addLayout(self.teamLayouts.Names[-1])

        for icon in range(Team.nPlayers):
            if icon%pXRow is 0:
                self.teamLayouts.Icons.append(QtGui.QHBoxLayout())
                self.teamLayouts.Icons[-1].addStretch(1)
                
            self.Teams[Team.id-1].Icons.append(playerIcon())
            self.Teams[Team.id-1].Icons[-1].Team = Team.id
             
            self.teamLayouts.Icons[-1].addWidget(self.Teams[Team.id-1].Icons[-1])
            self.teamLayouts.Icons[-1].addStretch(1)
            
            if icon%pXRow is (pXRow -1):
                self.teamLayouts.Teams[-1].addStretch(1)
                self.teamLayouts.Teams[-1].addLayout(self.teamLayouts.Icons[-1])
                
            if icon is (Team.nPlayers-1):
                self.teamLayouts.Teams[-1].addStretch(1)
                self.teamLayouts.Teams[-1].addLayout(self.teamLayouts.Icons[-1])
        
            fname = self.Teams[Team.id-1].Images[icon]
            info = QtCore.QFileInfo(fname)
            self.Teams[Team.id-1].Players.append(Player(fname))
            self.Teams[Team.id-1].Players[-1].setAcceptHoverEvents(True)
            self.Teams[Team.id-1].Players[-1].setAcceptDrops(True)
            self.Teams[Team.id-1].Players[-1].setPos(100+100,270)
            self.Teams[Team.id-1].Players[-1].rect = 0
            self.Teams[Team.id-1].Players[-1].drawBorder(redPen)
            self.Teams[Team.id-1].Players[-1].id = 100*Team.id+len(self.Teams[Team.id-1].Players)
            self.Teams[Team.id-1].Players[-1].Team = teamsName[Team.id-1]
            self.Teams[Team.id-1].Players[-1].Name = info.baseName()
            self.Teams[Team.id-1].Players[-1].itemID = len(self.Teams[Team.id-1].Players)
            self.Teams[Team.id-1].Icons[-1].itemID =  self.Teams[Team.id-1].Players[-1].itemID   
                
    def showDialog(self):
        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open Video','C:\Users\Bruno\workspace\MQF Trackers\src')
        if fname:
            self.Video.newFileName = fname
            self.Video.newFile = 1
    
    def saveP(self):
        
        pname, _ = QtGui.QFileDialog.getSaveFileName(self, 'Save Project','C:\Users\Bruno\workspace\MQF Tracker Pyside\src')
        info = QtCore.QFileInfo(pname)

        if pname and info.suffix() == "json":
            
            self.Video.Project.File     = QtCore.QFileInfo(pname)
            self.Video.Project.Name     = self.Video.Project.File.baseName() 
            self.Video.Project.Path     = self.Video.Project.File.path()
                        
            project = {'Video'  : self.Video.file,
                       'Teams'  : teamsName,
                       'nPlayer': nPlayers,
                       'Frame'  : self.Video.frame,
                       'lFrame' : self.Video.lastFrame,
                       }
            
            f = open(pname,'w')
            f.write(json.dumps(project))
            
            
            for team in range(len(teamsName)):
                names = []
                for p in range(self.Teams[team].nPlayers):
                    names.append(self.Teams[team].Players[p].Image)
                
                NAMES = {'Images': names}
                
                file = self.Video.Project.Path + '/' + teamsName[team] + ".json"
                temp = open(file,'w')
                temp.write(json.dumps(NAMES))
                

            self.Video.numPyFile   = self.Video.Project.Path + "/" + self.Video.Project.Name
            self.Video.matFile     = self.Video.Project.Path + "/" + self.Video.Project.Name + ".mat"
            
        
        elif pname:
            QtGui.QMessageBox.information(self,"Warning!", 'Projects must be saved using ".json" extension!')
    
    def loadP(self):
        pname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Load Project','C:\Users\Bruno\workspace\MQF Tracker Pyside\src')
        info = QtCore.QFileInfo(pname)

        if pname and info.suffix() == "json":
            
            self.Video.Project.File     = QtCore.QFileInfo(pname)
            self.Video.Project.Name     = self.Video.Project.File.baseName() 
            self.Video.Project.Path     = self.Video.Project.File.path()
            self.Video.numPyFile        = self.Video.Project.Path + "/" + self.Video.Project.Name
            self.Video.matFile          = self.Video.Project.Path + "/" + self.Video.Project.Name + ".mat"
            self.Video.defFiles       = 0
            
            with open(pname) as f:
                project = json.load(f)

            self.Video.frame = project['lFrame']
            
            self.Video.newFileName    = project['Video']
            self.Video.newFile  = 1
            
            try:
                temp = self.Video.numPyFile + ".npy"
                self.Video.trayectories = np.load(temp)
            except:
                print "No .py file."
                
            
            nPlayers = project['nPlayer']
            teamsName = project['Teams']
            for team in range(len(teamsName)):
                file = self.Video.Project.Path + '/' + teamsName[team] + ".json"
                with open(file) as f:
                    DATA = json.load(f)
                
                names = DATA['Images']
                self.Teams[team].Players = []
                for p in range(nPlayers):
                    fname = names[p]
                    info = QtCore.QFileInfo(fname)
                    self.Teams[team].Icons[p].fname = fname
                    self.Teams[team].Icons[p].setPixmap(QtGui.QPixmap(fname))
                    self.Teams[team].Players.append(Player(fname))
                    self.Teams[team].Players[p].Image = fname
                    self.Teams[team].Players[p].setAcceptHoverEvents(True)
                    self.Teams[team].Players[p].setAcceptDrops(True)
                    self.Teams[team].Players[p].setPos(100+100,270)
                    self.Teams[team].Players[p].rect = 0
                    self.Teams[team].Players[p].drawBorder(redPen)
                    self.Teams[team].Players[p].id = 100*(team+1)+len(self.Teams[team].Players)
                    self.Teams[team].Players[p].Team = teamsName[team]
                    self.Teams[team].Players[p].Name = info.baseName()
                    self.Teams[team].Players[p].itemID = len(self.Teams[team].Players)
                    self.Teams[team].Icons[p].itemID =  self.Teams[team].Players[p].itemID
                
                
            
    

        elif pname:
            QtGui.QMessageBox.information(self,"Warning!", 'You have to load an ".json" file!')

    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.Video.Run = 0
            self.Video.exit()
            self.Video.terminate()   
            event.accept()
        else:
            event.ignore() 
            
    def setFrame(self):
        self.Video.frame    = self.Video.sldVideo.value()
        self.Video.chgFrame       = 1
         
    def PlayStopVideo(self):
        if self.Video.file:
            if self.Video.Status:
                self.Video.Status = 0
                self.Video.pbTrack.setText("TRACK")
            else:
                self.Video.Status = 1
                self.Video.pbTrack.setText("STOP")

    def setSpeed(self):
        self.Video.spFactor = self.sldSpeed.value()/2.0+0.5
        self.lcdSpeed.display(self.Video.spFactor)
        
            
app = QtGui.QApplication(sys.argv)
app.setStyle(QtGui.QStyleFactory.create("cleanlooks"))
ex = eventsTracker()
ex.show()
ex.Scene.Field.drawBorder(grePenF)
sys.exit(app.exec_())